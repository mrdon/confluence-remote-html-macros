package services;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.httpclient.api.Response;
import com.atlassian.plugin.remotable.api.annotation.ComponentImport;
import com.atlassian.templaterenderer.annotations.HtmlSafe;
import com.google.common.base.Function;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import org.apache.commons.io.IOUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;
import static java.lang.Boolean.parseBoolean;
import static java.lang.Integer.parseInt;

/**
 *
 */
@Named
public class RssService
{
    @ComponentImport
    @Inject
    HttpClient httpClient;

    public Map<String, Object> createContext(SyndFeed feed,
                                             String url,
                                             Map<String, String[]> params
    )
    {
        final int maxItems = params.containsKey("max") ? parseInt(params.get("max")[0]) : 5;
        final boolean showTitlesOnly = !params.containsKey("showTitlesOnly") || parseBoolean(
                params.get("showTitlesOnly")[0]);
        final boolean showTitleBar = !params.containsKey("titleBar") || parseBoolean(
                params.get("titleBar")[0]);

        Map<String, Object> contextMap = newHashMap();
        contextMap.put("url", url);
        contextMap.put("feed", feed);
        contextMap.put("max", maxItems);
        contextMap.put("showTitlesOnly", showTitlesOnly);
        contextMap.put("showTitleBar", showTitleBar);
        contextMap.put("descriptionRenderer", new DescriptionRenderer());

        return contextMap;
    }

    public SyndFeed retrieveRSSFeed(final String url)
    {
        return httpClient.newRequest(url).get().<SyndFeed>transform().ok(new Function<Response, SyndFeed>()
                {
                    @Override
                    public SyndFeed apply(Response input)
                    {
                        return parseRSSFeed(url, input.getEntityStream());
                    }
                })
                .others(new Function<Response, SyndFeed>()
                {
                    @Override
                    public SyndFeed apply(Response input)
                    {
                        throw new FeedRetrievalException("Unexpected response status: " + input.getStatusCode());
                    }
                })
                .fail(new Function<Throwable, SyndFeed>()
                {
                    @Override
                    public SyndFeed apply(Throwable input)
                    {
                        throw new FeedRetrievalException(input);
                    }
                })
                .claim();
    }

    private SyndFeed parseRSSFeed(final String url, final InputStream inputStream) throws IllegalArgumentException
    {

        try
        {
            try
            {
                SyndFeedInput input = new SyndFeedInput();
                return input.build(new XmlReader(inputStream));
            }
            catch (IOException ioe)
            {
                throw new FeedException("Unable to read XML from " + url, ioe);
            }
        }
        catch (FeedException e)
        {
            throw new IllegalArgumentException(e);
        }
        finally
        {
            IOUtils.closeQuietly(inputStream);
        }
    }

    public static class DescriptionRenderer
    {
        @HtmlSafe
        public String render(SyndContent content)
        {
            return content.getValue();
        }
    }
}
