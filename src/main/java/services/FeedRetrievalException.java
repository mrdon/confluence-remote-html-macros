package services;

public class FeedRetrievalException extends RuntimeException
{
    public FeedRetrievalException()
    {
        super();
    }

    public FeedRetrievalException(String message)
    {
        super(message);
    }

    public FeedRetrievalException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public FeedRetrievalException(Throwable cause)
    {
        super(cause);
    }
}
