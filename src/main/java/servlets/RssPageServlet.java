package servlets;

import com.atlassian.plugin.remotable.kit.servlet.AbstractPageServlet;
import com.sun.syndication.feed.synd.SyndFeed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.RssService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Named
public class RssPageServlet extends AbstractPageServlet
{
    private static final Logger log = LoggerFactory.getLogger(RssPageServlet.class);

    @Inject
    private RssService rssService;

    @Override
    protected void doGet(final HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        final String rssUrl = req.getParameter("url");

        try
        {
            SyndFeed feed = rssService.retrieveRSSFeed(rssUrl);
            render("rss", req, resp, rssService.createContext(feed, rssUrl, req.getParameterMap()));
        }
        catch (RuntimeException ex)
        {
            log.warn("Exception retrieving and parsing RSS feed: " + ex.getMessage());
            log.error("Actual exception", ex);
            renderError("Unable to retrieve and render RSS feed: " + ex.getMessage(), req, resp);
        }
    }
}
