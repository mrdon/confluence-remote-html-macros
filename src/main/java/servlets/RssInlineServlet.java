package servlets;

import com.atlassian.plugin.remotable.api.annotation.ComponentImport;
import com.atlassian.plugin.remotable.api.service.RenderContext;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.sun.syndication.feed.synd.SyndFeed;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.FeedRetrievalException;
import services.RssService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.google.common.collect.Maps.newHashMap;

@Named
public class RssInlineServlet extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(RssInlineServlet.class);
    public static final int DEFAULT_MAX = 5;

    @Inject
    private RssService rssService;

    @ComponentImport
    @Inject
    TemplateRenderer templateRenderer;

    @ComponentImport
    @Inject
    RenderContext renderContext;

    @Override
    protected void doGet(final HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        final String rssUrl = getRssUrl(req);
        final Integer maxEntries = getMaxEntries(req);

        final String html;
        if (!isValidUrl(rssUrl) || maxEntries < 1)
        {
            try
            {
                final JSONObject errorObject = new JSONObject();
                final Collection<JSONObject> fieldErrors = new LinkedList<JSONObject>();
                if (!isValidUrl(rssUrl))
                {
                    fieldErrors.add(newFieldError("url", "must be a valid URL"));
                }
                if (maxEntries < 1)
                {
                    fieldErrors.add(newFieldError("max", "must be 1 or greater"));
                }
                if (!fieldErrors.isEmpty())
                {
                    errorObject.put("fields", new JSONArray(fieldErrors));
                }
                html = errorObject.toString();
                resp.setContentType("application/json");
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
            catch (JSONException e)
            {
                throw new ServletException(e);
            }
        }
        else
        {
            html = retrieveAndRender(rssUrl, req);
            resp.setContentType("text/xml");
            resp.setDateHeader("Expires", System.currentTimeMillis() + TimeUnit.HOURS.toMillis(1));
            resp.setHeader("Cache-Control", "public");
        }

        final byte[] data = html.getBytes(Charset.forName("UTF-8"));
        resp.setContentLength(data.length);
        resp.getOutputStream().write(data);
        resp.getOutputStream().close();
    }

    private boolean isValidUrl(String rssUrl)
    {
        try
        {
            new URI(rssUrl);
            return true;
        }
        catch (URISyntaxException ignore)
        {
            return false;
        }
    }

    private JSONObject newFieldError(String name, String message) throws JSONException
    {
        final JSONObject fieldError = new JSONObject();
        fieldError.put("name", name);
        fieldError.put("message", message);
        return fieldError;
    }

    private Integer getMaxEntries(HttpServletRequest req)
    {
        try
        {
            String macroMax = req.getParameter("max");
            return macroMax != null ? Integer.valueOf(macroMax) : DEFAULT_MAX;
        }
        catch (NumberFormatException ignore)
        {
            return -1;
        }
    }

    private String getRssUrl(HttpServletRequest req)
    {
        return req.getParameter("url");
    }

    private String retrieveAndRender(String rssUrl, HttpServletRequest req)
    {
        try
        {
            SyndFeed feed = rssService.retrieveRSSFeed(rssUrl);
            Map<String, Object> ctx = newHashMap(renderContext.toContextMap());
            ctx.putAll(rssService.createContext(feed, rssUrl, req.getParameterMap()));
            return renderFeedInline(ctx);
        }
        catch (FeedRetrievalException ex)
        {
            log.debug("Exception retrieving and parsing RSS feed: " + ex.getMessage(), ex);
            return "Unable to retrieve and render RSS feed";
        }
        catch (RuntimeException ex)
        {
            log.warn("Unexpected exception retrieving and parsing RSS feed: " + ex.getMessage(), ex);
            return "Unable to retrieve and render RSS feed";
        }
    }

    private String renderFeedInline(Map<String, Object> contextMap)
    {
        try
        {
            StringWriter writer = new StringWriter();
            templateRenderer.render("views/rss.vm", contextMap, writer);
            return writer.toString();
        }
        catch (Exception e)
        {
            log.error("Error while trying to assemble the RSS result!", e);
            return "Error rendering feed: " + e.getMessage();
        }
    }
}
